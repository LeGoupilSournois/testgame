While playing this game please follow these step to check every functionnality:

(the images are located in the Screen folder)


STEP 1 : launch TestGame.exe and choose your resolution

STEP 2 : once the screen is loaded click on 'i am an adult', 'yes i am sure'

STEP 3: you should here a music playing from now on


STEP 4: you are now in the main screen. it should look like the Screen/MainScreen.png

STEP 5 : click on Start New Game, then click on next Until you load the MainLevel

STEP 6: the main level should look like this image : Screen/MainLevel.png

STEP 7: TO move use WASD or click on a tile (there is a 3seconds delay for the click)

STEP 9: check your quest list by clicking on the book icon (Screen/QuestButton)

STEP 10: The Quest List should be empty, move to this tile (Screen/MoveToTile)

STEP 11: When you Reach the tile tis message should appear : (Screen/Message)

STEP: 12: Open your quest journal (Again),you should see this :(Screen/Quest2)

STEP 13: Click on the Character, and click on talk
			You should see Hello, click again and say goodbye to
			end the conversation (Screen/Dialog)
			
STEP 14: press echap click save game, enter a name of you choosing and save the game

STEP 14: go to the main menu and exit the game

Step 15: go to your user folder, usually it is : C:\Users\UserName
Step 16: go to Documents (C:\Users\UserName\Documents)
Step 17: you should see a folder called "LeGoupilSournois"
step 18: go to LeGoupilSournois/TestGame/Save (C:\Users\UserName\Documents\LeGoupilSournois\TestGame\Save)
step 19: you should see your save game previously done in step 14
step 20: you should be fine playing my following games